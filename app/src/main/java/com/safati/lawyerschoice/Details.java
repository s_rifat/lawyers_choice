package com.safati.lawyerschoice;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class Details extends AppCompatActivity {
    private Button btn, btnScroll;
    private LinearLayout llPdf;
    private Bitmap bitmap;


    //private AppCompatEditText letterNoInput, dateInput, bankNameInput, bankAddressInput,branchNameInput,clientNameInput;
    //String stringName[] = new String[6];
   public String letterNoStore, dateStore, bankNameStore, bankAddressStore,branchNameStore,clientNameStore;
    private Button saveButton;
    //TextView letterNoView, dateView, bankNameView, bankAddressView,branchNameView,clientNameView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        btn = findViewById(R.id.makePdfID);

        llPdf = findViewById(R.id.llpdf);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("size"," "+llPdf.getWidth() +"  "+llPdf.getWidth());
                bitmap = loadBitmapFromView(llPdf, llPdf.getWidth(), llPdf.getHeight());
                createPdf();
            }
        });

        show();



    }

    public static Bitmap loadBitmapFromView(View v, int width, int height) {
        Bitmap b = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.draw(c);

        return b;
    }

    private void createPdf(){
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        //  Display display = wm.getDefaultDisplay();
        DisplayMetrics displaymetrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        float hight = displaymetrics.heightPixels ;
        float width = displaymetrics.widthPixels ;

        int convertHighet = (int) hight, convertWidth = (int) width;

//        Resources mResources = getResources();
//        Bitmap bitmap = BitmapFactory.decodeResource(mResources, R.drawable.screenshot);

        PdfDocument document = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(convertWidth, convertHighet, 1).create();
        PdfDocument.Page page = document.startPage(pageInfo);

        Canvas canvas = page.getCanvas();

        Paint paint = new Paint();
        canvas.drawPaint(paint);

        bitmap = Bitmap.createScaledBitmap(bitmap, convertWidth, convertHighet, true);

        paint.setColor(Color.BLUE);
        canvas.drawBitmap(bitmap, 0, 0 , null);
        document.finishPage(page);

        // write the document content
        String targetPdf = "/sdcard/pdffromlayout.pdf";
        File filePath;
        filePath = new File(targetPdf);
        try {
            document.writeTo(new FileOutputStream(filePath));

        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Something wrong: " + e.toString(), Toast.LENGTH_LONG).show();
        }

        // close the document
        document.close();
        Toast.makeText(this, "PDF is created!!!", Toast.LENGTH_SHORT).show();

        openGeneratedPDF();

    }

    private void openGeneratedPDF(){
        //Uri urll = FileProvider.getUriForFile(Details.this,Details.this.getApplicationContext().getPackageName() + ".my.package.name.provider" );
        File file = new File("/sdcard/pdffromlayout.pdf");
        if (file.exists())
        {
            Intent intent=new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.fromFile(file);
            intent.setDataAndType(uri, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            try
            {
                startActivity(intent);
            }
            catch(ActivityNotFoundException e)
            {
                Toast.makeText(Details.this, "No Application available to view pdf", Toast.LENGTH_LONG).show();
            }
        }
    }




    public void show() {


//                letterNoStore = hm.letterNoInput.getText().toString();
//                dateStore = "Date:" + hm.dateInput.getText().toString();
//                bankNameStore = hm.bankNameInput.getText().toString();
//                bankAddressStore = hm.bankAddressInput.getText().toString();
//                branchNameStore = hm.branchNameInput.getText().toString();
//                clientNameStore = hm.clientNameInput.getText().toString();

        TextView letterNoView =  findViewById(R.id.letterNoId2);
        TextView dateView =  findViewById(R.id.dateId2);
        TextView txt = findViewById(R.id.textViewId2);

        Intent intent = getIntent();

        letterNoStore = intent.getStringExtra("letterNoStore");
        dateStore =  intent.getStringExtra("dateStore");

        bankNameStore = intent.getStringExtra("bankNameStore");
        bankAddressStore =  intent.getStringExtra("bankAddressStore");

        branchNameStore = intent.getStringExtra("branchNameStore");
        clientNameStore= intent.getStringExtra("clientNameStore");



        System.out.println("latterno = " +letterNoStore + "  Date: "+dateStore);



        String subject = "Subject: Legal opinion upon vetting of the property documents standing in the name of " + clientNameStore + "; A/c. " + clientNameStore+".";
        String str = "To" + "\n" + "Head of the Branch" + "\n" + bankNameStore+ ",\n"  + branchNameStore + "\n" + bankAddressStore + "\n\n" + subject +"\n\n" + "Dear Sir," + "\n" +
                "In reference to your latter no. " + letterNoStore + " dated " + dateStore +
                " regarding the above captioned subject and as well as our previous correspondence with you; the Bank has forwarded us deeds & documents on account of " +
                clientNameStore + " for vetting and legal opinion from the Chamber's end." + "\n\n" +
                "Upon perusal of the referred deeds & documents, a complete opinion on account of the captioned client of the Bank has been prepared in a manner as follows:"
                ;
        // TextView bankNameView =

       letterNoView.setText(letterNoStore);
        dateView.setText(dateStore);
        txt.setText(str);


//        txt.setMovementMethod(new ScrollingMovementMethod());

            }

    }

//    public void showData(){
//        letterNoView.setText("FHYJJJJJJJ");
//        dateView.setText(dateStore);
//    }

