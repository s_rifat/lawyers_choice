package com.safati.lawyerschoice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;
import android.widget.Button;

public class home extends AppCompatActivity {

    //private Button save;
    String letterNoStore, dateStore, bankNameStore, bankAddressStore,branchNameStore,clientNameStore;

    public AppCompatEditText letterNoInput, dateInput, bankNameInput, bankAddressInput,branchNameInput,clientNameInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

       // save = findViewById(R.id.saveButtonId);

      //  saveButton = findViewById(R.id.saveButtonId);




    }

    public String getLetterNoStore() {
        return letterNoStore;
    }

    public void setLetterNoStore(String letterNoStore) {
        this.letterNoStore = letterNoStore;
    }

    public void save(View v)
    {
        letterNoInput = findViewById(R.id.letterNoId);
        dateInput = findViewById(R.id.dateId);
        bankNameInput = findViewById(R.id.bankNameId);
        bankAddressInput = findViewById(R.id.bankAddressId);
        branchNameInput = findViewById(R.id.branchNameId);
        clientNameInput = findViewById(R.id.clintNameId);
       // Details details = new Details();

       setLetterNoStore(letterNoInput.getText().toString());

        System.out.println("Here .................... Letter no =============== " + getLetterNoStore());

        Intent in = new Intent(this, Details.class);
        in.putExtra("letterNoStore",letterNoInput.getText().toString());
        in.putExtra("dateStore",dateInput.getText().toString());

        in.putExtra("bankNameStore", bankNameInput.getText().toString());
        in.putExtra("bankAddressStore",bankAddressInput.getText().toString());

        in.putExtra("branchNameStore", branchNameInput.getText().toString());
        in.putExtra("clientNameStore", clientNameInput.getText().toString());

        finish();
        startActivity(in);


       // System.out.println("letter no: "+letterNoInput.getText().toString());

    }

}
